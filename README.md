![screenshot](https://cdn.pbrd.co/images/HQZVyKM.png)

Revolut Currencies App
- Made with ❤️
- 100% vanilla swift (No carthage, cocoapods or external libraries)

PS: I avoided RxSwift and Singletons. So I used NotificationCenter to propagate messages