//
//  CurrenciesListViewModelTest.swift
//  RevolutCurrenciesTests
//
//  Created by Rodrigo Dumont on 09/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import XCTest
@testable import RevolutCurrencies

class CurrenciesListViewModelTest: XCTestCase {

    var sut: CurrenciesListViewModelMock?

    override func setUp() {
        super.setUp()
        sut = CurrenciesListViewModelMock()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testSelectedRate() {
        // WHEN
        sut?.getCurrencies()

        // THEN
        XCTAssertTrue(sut?.didCallGetCurrencies ?? false, "Fail get currencies")
    }

}

class CurrenciesListViewModelMock: CurrenciesListViewModel, CurrenciesListViewModelDelegate {
    var didCallGetCurrencies = false

    public override func getCurrencies() {
        ratesUpdated()
    }

    func ratesUpdated() {
        didCallGetCurrencies = true
    }
}
