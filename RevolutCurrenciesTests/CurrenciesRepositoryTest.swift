//
//  CurrenciesRepositoryTest.swift
//  RevolutCurrenciesTests
//
//  Created by Rodrigo Dumont on 09/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import XCTest
@testable import RevolutCurrencies

class CurrenciesRepositoryTest: XCTestCase {

    var repository: CurrenciesRepositoryMock?

    override func setUp() {
        super.setUp()
        repository = CurrenciesRepositoryMock()
    }

    override func tearDown() {
        repository = nil
        super.tearDown()
    }

    func testGetCurrencies() {
        // GIVEN
        let expectation = self.expectation(description: "GetCurrencies")
        var base: String?
        var rates: [String: Double]?

        // WHEN
        repository?.getCurrencies(base: "EUR", then: { (currenciesList) in
            base = currenciesList?.base
            rates = currenciesList?.rates
            expectation.fulfill()
        })

        // THEN
        waitForExpectations(timeout: 1)
        XCTAssertEqual(base, "EUR")
        XCTAssertNotNil(rates)
    }

}

class CurrenciesRepositoryMock: CurrenciesRepository {
    override func getCurrencies(base: String, then: @escaping (CurrenciesList?) -> Void) {
        return then(CurrenciesList(base: "EUR", date: "", rates: ["EUR": 1.0]))
    }
}
