//
//  CurrencyCellViewModelTest.swift
//  RevolutCurrenciesTests
//
//  Created by Rodrigo Dumont on 09/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import XCTest
@testable import RevolutCurrencies

class CurrencyCellViewModelTest: XCTestCase {

    var sut: CurrencyCellViewModel?

    override func setUp() {
        super.setUp()
        sut = CurrencyCellViewModel()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testSelectRate() {
        // GIVEN
        sut?.rate = Rate(name: "EUR", value: 1.0)

        // WHEN
        sut?.selectRate()

        // THEN
        XCTAssertEqual(sut?.selectedRate?.name, sut?.rate?.name)
        XCTAssertEqual(sut?.selectedRate?.value, sut?.rate?.value)
    }

    func testPrice() {
        // GIVEN
        sut?.rate = Rate(name: "EUR", value: 1.0)
        sut?.selectedRate = Rate(name: "EUR", value: 1.0)

        // WHEN
        let price = sut?.price

        // THEN
        XCTAssertEqual(price, 1.0)
    }

    func testRatesUpdated() {
        // GIVEN
        sut?.rate = Rate(name: "EUR", value: 1.0)
        sut?.selectedRate = Rate(name: "EUR", value: 1.0)
        let updatedRate = Rate(name: "EUR", value: 4.20)

        // WHEN
        NotificationCenter.default.post(name: .ratesUpdated, object: [updatedRate])

        // THEN
        XCTAssertEqual(sut?.rate?.name, updatedRate.name)
        XCTAssertEqual(sut?.rate?.value, updatedRate.value)
    }

}
