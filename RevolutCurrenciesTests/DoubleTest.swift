//
//  DoubleTest.swift
//  RevolutCurrenciesTests
//
//  Created by Rodrigo Dumont on 09/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import XCTest
@testable import RevolutCurrencies

class DoubleTest: XCTestCase {

    var double: Double?

    override func setUp() {
        super.setUp()
        double = 4.20
    }

    override func tearDown() {
        double = nil
        super.tearDown()
    }

    func testToCurrency() {
        // GIVEN
        double = 4.20

        // WHEN
        let result = double?.toCurrency()

        // THEN
        XCTAssertEqual(result, "4.20")
    }

}
