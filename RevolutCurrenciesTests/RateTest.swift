//
//  RateTest.swift
//  RevolutCurrenciesTests
//
//  Created by Rodrigo Dumont on 09/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import XCTest
@testable import RevolutCurrencies

class RateTest: XCTestCase {

    var rate: Rate?

    override func setUp() {
        super.setUp()
        rate = Rate(name: "", value: 0.0)
    }

    override func tearDown() {
        rate = nil
        super.tearDown()
    }


    func testExtraImageEUR() {
        // GIVEN
        rate = Rate(name: "EUR", value: 1.0)

        // WHEN
        let image = rate?.extra.image

        // THEN
        XCTAssertEqual(image, UIImage(named: "icEUR"))
    }

    func testExtraNameEUR() {
        // GIVEN
        rate = Rate(name: "EUR", value: 1.0)

        // WHEN
        let name = rate?.extra.name

        // THEN
        XCTAssertEqual(name, "Euro")
    }

}
