//
//  CurrencyCell.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {

    @IBOutlet weak var flagImageView: UIImageView! {
        didSet {
            flagImageView.layer.masksToBounds = true
            flagImageView.layer.cornerRadius = flagImageView.frame.height / 2.0
            flagImageView.layer.borderColor = UIColor.black.withAlphaComponent(0.50).cgColor
            flagImageView.layer.borderWidth = 0.5
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!

    let viewModel = CurrencyCellViewModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        viewModel.delegate = self
        priceTextField.delegate = self
        priceTextField.addTarget(self, action: #selector(priceTextFieldChanged), for: .editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        priceTextField.isEnabled = isSelected
    }

    // MARK: - Actions
    func clickOnContentView() {
        priceTextField.becomeFirstResponder()
        viewModel.selectRate()
    }

    // MARK: - Helpers
    func updateFlagImageView(image: UIImage?) {
        flagImageView.image = image
    }

    func update(title: String?, subtitle: String?) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }

    func updatePriceLabel(price: Double) {
        if !priceTextField.isEditing {
            priceTextField.text = price.toCurrency()
        }
    }
}

// MARK: - CurrencyCellViewModelDelegate
extension CurrencyCell: CurrencyCellViewModelDelegate {
    func updateUI() {
        updateFlagImageView(image: viewModel.rate?.extra.image)
        update(title: viewModel.rate?.name, subtitle: viewModel.rate?.extra.name)
        updatePriceLabel(price: viewModel.price)
    }
}

// MARK: - UITextFieldDelegate
extension CurrencyCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        return Double(text) != nil || text == ""
    }

    @objc func priceTextFieldChanged() -> Bool {
        viewModel.selectedRate?.value = Double(priceTextField.text ?? "0") ?? 0.0
        NotificationCenter.default.post(name: .priceUpdated, object: viewModel.selectedRate)
        return true
    }
}
