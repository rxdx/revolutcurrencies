
//
//  CurrencyCellViewModel.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

protocol CurrencyCellViewModelDelegate {
    func updateUI()
}

class CurrencyCellViewModel {

    var delegate: CurrencyCellViewModelDelegate?
    var selectedRate: Rate? {
        didSet {
            delegate?.updateUI()
        }
    }
    var rate: Rate? {
        didSet {
            delegate?.updateUI()
        }
    }
    var isSelected: Bool {
        return selectedRate?.name == rate?.name
    }
    var price: Double {
        if isSelected {
            return selectedRate?.value ?? 1.0
        } else {
            return (rate?.value ?? 1.0) * (selectedRate?.value ?? 1.0)
        }
    }

    init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ratesUpdated(_:)),
                                               name: .ratesUpdated,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(priceUpdated(_:)),
                                               name: .priceUpdated,
                                               object: nil)
    }

    func selectRate() {
        let selectedRate = Rate(name: rate?.name ?? "???", value: price)
        NotificationCenter.default.post(name: .priceUpdated, object: selectedRate)
    }
}

// MARK: - Notifications
extension CurrencyCellViewModel {
    @objc func ratesUpdated(_ sender: Notification) {
        if let rates = sender.object as? [Rate], let rate = rates.first(where: { $0.name == self.rate?.name }) {
            self.rate?.value = rate.value
        }
        delegate?.updateUI()
    }

    @objc func priceUpdated(_ sender: Notification) {
        selectedRate = sender.object as? Rate
        delegate?.updateUI()
    }
}

