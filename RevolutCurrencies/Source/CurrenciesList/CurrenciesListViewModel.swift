//
//  CurrenciesListViewModel.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

protocol CurrenciesListViewModelDelegate {
    func ratesUpdated()
}

public class CurrenciesListViewModel {

    var delegate: CurrenciesListViewModelDelegate? {
        didSet {
            setupTimer()
        }
    }
    var rates = [Rate]()
    var timer: Timer?
    var selectedRate: Rate? {
        didSet {
            getCurrencies()
        }
    }

    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(priceUpdate), name: .priceUpdated, object: nil)
    }

    func getCurrencies() {
        let baseName = selectedRate?.name ?? "EUR"
        let basePrice = selectedRate?.value ?? 1.0
        CurrenciesRepository().getCurrencies(base: baseName) { [weak self] (currenciesList) in
            currenciesList?.rates?.forEach({ (rate) in
                let (name, value) = rate

                if let index = self?.rates.firstIndex(where: { $0.name == name }) {
                    self?.rates[index].value = value
                } else {
                    self?.rates.append(Rate(name: name, value: value))
                }
            })

            if let index = self?.rates.firstIndex(where: { $0.name == baseName }) {
                self?.rates[index].value = 1.0
            } else {
                self?.rates.insert(Rate(name: baseName, value: basePrice), at: 0)
            }

            self?.delegate?.ratesUpdated()
            NotificationCenter.default.post(name: .ratesUpdated, object: self?.rates, userInfo: nil)
        }
    }

    func setupTimer(interval: TimeInterval = 1) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { [weak self] _ in
            self?.getCurrencies()
        }
        timer?.fire()
    }

    @objc func priceUpdate(notification: Notification) {
        guard let selectedRate = notification.object as? Rate else {
            return
        }
        self.selectedRate = selectedRate
        if let index = rates.firstIndex(where: { $0.name == selectedRate.name }) {
            let rate = rates.remove(at: index)
            rates.insert(rate, at: 0)
        }
    }

}
