//
//  CurrenciesListViewController.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

class CurrenciesListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let viewModel = CurrenciesListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
    }
}

// MARK: - UITableViewDataSource
extension CurrenciesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rates.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath) as? CurrencyCell else {
            return UITableViewCell()
        }

        cell.viewModel.rate = viewModel.rates[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CurrenciesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? CurrencyCell {
            cell.clickOnContentView()
            tableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: 0))
            tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
}

// MARK: - CurrenciesListViewModelDelegate
extension CurrenciesListViewController: CurrenciesListViewModelDelegate {
    func ratesUpdated() {
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.reloadData()
        }
    }
}
