//
//  Double+Extensions.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 07/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

extension Double {

    func toCurrency() -> String {
        return String(format: "%.02f", self)
    }
}
