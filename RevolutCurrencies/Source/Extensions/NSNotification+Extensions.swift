//
//  NSNotification+Extensions.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

extension NSNotification.Name {
    static let ratesUpdated = NSNotification.Name(rawValue: "RatesUpdated")
    static let priceUpdated = NSNotification.Name(rawValue: "PriceUpdated")
}
