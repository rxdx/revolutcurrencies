//
//  CurrenciesList.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

struct CurrenciesList: Decodable {
    var base: String?
    var date: String?
    var rates: [String: Double]?
}
