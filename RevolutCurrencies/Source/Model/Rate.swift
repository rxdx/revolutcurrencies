//
//  Rate.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 08/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

struct Rate {
    var name: String
    var value: Double
}

import UIKit
extension Rate {
    var extra: (image: UIImage?, name: String?) {
        switch name {
        case "AUD":
            return (image: UIImage(named: "ic\(name)"), name: "Australian Dollar")
        case "BGN":
            return (image: UIImage(named: "ic\(name)"), name: "Bulgarian Lev")
        case "BRL":
            return (image: UIImage(named: "ic\(name)"), name: "Brazilian Real")
        case "CAD":
            return (image: UIImage(named: "ic\(name)"), name: "Canadian Dollar")
        case "CHF":
            return (image: UIImage(named: "ic\(name)"), name: "Swiss Franc")
        case "CNY":
            return (image: UIImage(named: "ic\(name)"), name: "Chinese Yuan Renminbi")
        case "CZK":
            return (image: UIImage(named: "ic\(name)"), name: "Czech Crown")
        case "DKK":
            return (image: UIImage(named: "ic\(name)"), name: "Danish Krone")
        case "EUR":
            return (image: UIImage(named: "ic\(name)"), name: "Euro")
        case "GBP":
            return (image: UIImage(named: "ic\(name)"), name: "Pound Sterling")
        case "HKD":
            return (image: UIImage(named: "ic\(name)"), name: "Hong Kong Dollar")
        case "HRK":
            return (image: UIImage(named: "ic\(name)"), name: "Croatian Kuna")
        case "HUF":
            return (image: UIImage(named: "ic\(name)"), name: "Hungarian Forint")
        case "IDR":
            return (image: UIImage(named: "ic\(name)"), name: "Indonesian Rupiah")
        case "ILS":
            return (image: UIImage(named: "ic\(name)"), name: "Israeli New Shequel")
        case "INR":
            return (image: UIImage(named: "ic\(name)"), name: "Indian Rupee")
        case "ISK":
            return (image: UIImage(named: "ic\(name)"), name: "Icelandic Krona")
        case "JPY":
            return (image: UIImage(named: "ic\(name)"), name: "Japanese Yen")
        case "KRW":
            return (image: UIImage(named: "ic\(name)"), name: "Korean Won")
        case "MXN":
            return (image: UIImage(named: "ic\(name)"), name: "Mexican Peso")
        case "MYR":
            return (image: UIImage(named: "ic\(name)"), name: "Malaysian Ringgit")
        case "NOK":
            return (image: UIImage(named: "ic\(name)"), name: "Norwegian Krone")
        case "NZD":
            return (image: UIImage(named: "ic\(name)"), name: "New Zealand Dollar")
        case "PHP":
            return (image: UIImage(named: "ic\(name)"), name: "Philippine Peso")
        case "PLN":
            return (image: UIImage(named: "ic\(name)"), name: "Polish Zloty")
        case "RON":
            return (image: UIImage(named: "ic\(name)"), name: "Romanian Leu")
        case "RUB":
            return (image: UIImage(named: "ic\(name)"), name: "Russian Ruble")
        case "SEK":
            return (image: UIImage(named: "ic\(name)"), name: "Swedish Krona")
        case "SGD":
            return (image: UIImage(named: "ic\(name)"), name: "Singapore Dollar")
        case "THB":
            return (image: UIImage(named: "ic\(name)"), name: "Baht Thai")
        case "TRY":
            return (image: UIImage(named: "ic\(name)"), name: "Turkish Lira")
        case "USD":
            return (image: UIImage(named: "ic\(name)"), name: "United States Dollar")
        case "ZAR":
            return (image: UIImage(named: "ic\(name)"), name: "South African Rand")
        default:
            return (image: nil, name: nil)
        }
    }
}
