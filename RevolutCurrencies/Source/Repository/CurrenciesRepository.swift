//
//  CurrenciesRepository.swift
//  RevolutCurrencies
//
//  Created by Rodrigo Dumont on 06/12/18.
//  Copyright © 2018 Rodrigo Dumont. All rights reserved.
//

import UIKit

class CurrenciesRepository {

    func getCurrencies(base: String = "EUR", then: @escaping (CurrenciesList?) -> Void) {
        guard let url = URL(string: "https://revolut.duckdns.org/latest?base=\(base)") else {
            then(nil)
            return
        }
        URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
            guard let data = data, let currenciesList = try? JSONDecoder().decode(CurrenciesList.self, from: data) else {
                DispatchQueue.main.async {
                    then(nil)
                }
                return
            }
            DispatchQueue.main.async {
                then(currenciesList)
            }
        }.resume()
    }

}
